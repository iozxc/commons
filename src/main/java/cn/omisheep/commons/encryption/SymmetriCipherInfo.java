/**
 * Copyright 2015 sp42 frank@ajaxjs.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.omisheep.commons.encryption;


import cn.omisheep.commons.util.LogHelper;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.*;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;

/**
 * @author zhouxinchen[1269670415@qq.com]
 * @since 1.0.5
 */
public class SymmetriCipherInfo {

    private SymmetriCipherInfo() {
        throw new UnsupportedOperationException();
    }

    private static final LogHelper log = LogHelper.getLog(SymmetriCipherInfo.class);

    /**
     * 加密算法
     */
    private String algorithm;

    /**
     * 密钥长度
     */
    private int keySize;

    /**
     * 创建一个 CipherInfo 实例
     *
     * @param algorithm 加密算法
     * @param keySize   密钥长度
     */
    public SymmetriCipherInfo(String algorithm, int keySize) {
        this.algorithm = algorithm;
        this.keySize   = keySize;
    }

    /**
     * 进行加密或解密
     *
     * @param algorithm 选择的算法
     * @param mode      是解密模式还是加密模式？
     * @param key       密钥
     * @param params    参数，可选的
     * @param s         输入的内容
     * @return 结果
     */
    public static byte[] doCipher(String algorithm, int mode, Key key, AlgorithmParameterSpec params, byte[] s) {
        try {
            Cipher cipher = Cipher.getInstance(algorithm);

            if (params != null)
                try {
                    cipher.init(mode, key, params);
                } catch (InvalidAlgorithmParameterException e) {
                    log.error(e);
                }
            else
                cipher.init(mode, key);

            /*
             * 为了防止解密时报 javax.crypto.IllegalBlockSizeException: Input length must be
             * multiple of 8 when decrypting with padded cipher 异常， 不能把加密后的字节数组直接转换成字符串
             */
            return cipher.doFinal(s);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            log.error(e);
            return null;
        }
    }

    /**
     * AES/DES 专用
     *
     * @param ci    密码信息
     * @param mode  是解密模式还是加密模式？
     * @param key   密钥
     * @param bytes 输入的内容，可以是字符串转换 byte[]
     * @return 转换后的内容
     */
    static byte[] doCipher(SymmetriCipherInfo ci, int mode, String key, byte[] bytes) {
        SecretKey _key;// 获得密钥对象

        try {
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            sr.setSeed(key.getBytes());

            _key = getSecretKey(ci.getCipherAlgorithm(), sr, ci.getKeySize());// 生成密钥
        } catch (NoSuchAlgorithmException e) {
            log.error(e);
            return null;
        }

        return doCipher(ci.getCipherAlgorithm(), mode, _key, null, bytes);
    }

    /**
     * 获取对称加密用的 SecretKey
     *
     * @param algorithm 加密算法
     * @param secure    可选的
     * @param keySize   可选的
     * @return SecretKey
     */
    public static SecretKey getSecretKey(String algorithm, SecureRandom secure, Integer keySize) {
        KeyGenerator generator;

        try {
            generator = KeyGenerator.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            log.error(e);
            return null;
        }

        if (secure != null) {
            if (keySize == null)
                generator.init(secure);
            else
                generator.init(keySize, secure);
        }

        return generator.generateKey();
    }

    public static String getSecretKey(String algorithm, SecureRandom secure) {
        try {
            return Base64.encodeBase64String(getSecretKey(algorithm, secure, null).getEncoded());
        } catch (Exception e) {
            log.error(e);
            return "";
        }

    }

    public int getKeySize() {
        return keySize;
    }

    public void setKeySize(int keySize) {
        this.keySize = keySize;
    }

    public String getCipherAlgorithm() {
        return algorithm;
    }

    public void setCipherAlgorithm(String cipherAlgorithm) {
        this.algorithm = cipherAlgorithm;
    }

}
