package cn.omisheep.commons.util;

/**
 * @author zhouxinchen[1269670415@qq.com]
 * @since 1.0.0
 */
public class Assert {

    private Assert() {
        throw new UnsupportedOperationException();
    }

    public static void isTrue(boolean expression,
                              String message) {
        if (!expression) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void state(boolean expression,
                             String message) {
        if (!expression) {
            throw new IllegalStateException(message);
        }
    }

    public static void isNull(Object object,
                              String message) {
        if (object != null) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void notNull(Object object,
                               String message) {
        if (object == null) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void hasText(String text,
                               String message) {
        if (!StringUtils.hasText(text)) {
            throw new IllegalArgumentException(message);
        }
    }
}
