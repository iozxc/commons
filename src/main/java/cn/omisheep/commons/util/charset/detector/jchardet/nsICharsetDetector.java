package cn.omisheep.commons.util.charset.detector.jchardet;

/**
 * @author zhouxinchen[1269670415@qq.com]
 * @since 1.0.8
 */
public interface nsICharsetDetector {

    void Init(nsICharsetDetectionObserver observer);

    boolean DoIt(byte[] aBuf,
                 int aLen,
                 boolean oDontFeedMe);

    void Done();
}

