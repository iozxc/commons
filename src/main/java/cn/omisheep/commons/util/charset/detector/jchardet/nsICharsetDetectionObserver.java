package cn.omisheep.commons.util.charset.detector.jchardet;

import java.lang.*;

/**
 * @author zhouxinchen[1269670415@qq.com]
 * @since 1.0.8
 */
public interface nsICharsetDetectionObserver {

    void Notify(String charset);
}

