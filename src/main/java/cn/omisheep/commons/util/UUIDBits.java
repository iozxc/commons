package cn.omisheep.commons.util;


import java.util.UUID;
import java.util.function.Function;

/**
 * uuid生成工具类
 *
 * @author zhouxinchen[1269670415@qq.com]
 * @since 1.0.5
 */
public class UUIDBits {

    private UUIDBits() {
        throw new UnsupportedOperationException();
    }

    private static final char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray();

    private char[] encode(byte[] data) {
        char[]  out = new char[((data.length + 2) / 3) * 4];
        boolean quad, trip;
        for (int i = 0, index = 0; i < data.length; i += 3, index += 4) {
            quad = trip = false;
            int val = (0xFF & (int) data[i]);
            val <<= 8;
            if ((i + 1) < data.length) {
                val |= (0xFF & (int) data[i + 1]);
                trip = true;
            }
            val <<= 8;
            if ((i + 2) < data.length) {
                val |= (0xFF & (int) data[i + 2]);
                quad = true;
            }
            out[index + 3] = alphabet[(quad ? (val & 0x3F) : 64)];
            val >>= 6;
            out[index + 2] = alphabet[(trip ? (val & 0x3F) : 64)];
            val >>= 6;
            out[index + 1] = alphabet[val & 0x3F];
            val >>= 6;
            out[index]     = alphabet[val & 0x3F];
        }
        return out;
    }

    private byte[] toBytes() {
        UUID   uuid   = UUID.randomUUID();
        long   msb    = uuid.getMostSignificantBits();
        long   lsb    = uuid.getLeastSignificantBits();
        byte[] buffer = new byte[16];

        for (int i = 0; i < 8; i++) {
            buffer[i]     = (byte) ((msb >>> 8 * (7 - i)) & 0xFF);
            buffer[i + 8] = (byte) ((lsb >>> 8 * (7 - i)) & 0xFF);
        }
        return buffer;
    }

    public String getUUID() {
        char[] res = encode(toBytes());
        return new String(res, 0, res.length - 2);
    }

    public static String getUUIDBits(int bits) {
        if (bits > 22) return getUUIDBits(22) + getUUIDBits(bits - 22);
        UUID   uuid = UUID.randomUUID();
        long   msb  = uuid.getMostSignificantBits();
        long   lsb  = uuid.getLeastSignificantBits();
        char[] out  = new char[24];
        int    tmp  = 0, idx = 0;
        int    bit  = 0, bt1 = 8, bt2 = 8;
        int    mask = 0x00, offsetm = 0, offsetl = 0;

        for (; bit < 16; bit += 3, idx += 4) {
            offsetm = 64 - (bit + 3) * 8;
            tmp     = 0;

            if (bt1 > 3) {
                mask = (1 << 8 * 3) - 1;
            } else if (bt1 >= 0) {
                mask = (1 << 8 * bt1) - 1;
                bt2 -= 3 - bt1;
            } else {
                mask = (1 << 8 * (Math.min(bt2, 3))) - 1;
                bt2 -= 3;
            }
            if (bt1 > 0) {
                bt1 -= 3;
                tmp = (int) ((offsetm < 0) ? msb : (msb >>> offsetm) & mask);
                if (bt1 < 0) {
                    tmp <<= Math.abs(offsetm);
                    mask = (1 << 8 * Math.abs(bt1)) - 1;
                }
            }
            if (offsetm < 0) {
                offsetl = 64 + offsetm;
                tmp |= ((offsetl < 0) ? lsb : (lsb >>> offsetl)) & mask;
            }

            if (bit == 15) {
                out[idx + 3] = alphabet[alphabet.length - 1];
                out[idx + 2] = alphabet[alphabet.length - 1];
                tmp <<= 4;
            } else {
                out[idx + 3] = alphabet[tmp & 0x3d];
                tmp >>= 6;
                out[idx + 2] = alphabet[tmp & 0x3d];
                tmp >>= 6;
            }
            out[idx + 1] = alphabet[tmp & 0x3d];
            tmp >>= 6;
            out[idx]     = alphabet[tmp & 0x3d];
        }

        return new String(out, 0, bits);
    }

    public static String getUUIDBits(int bits,
                                     Function<String, Boolean> ok) {
        return getUUIDBits(bits, ok, 10);
    }

    public static String getUUIDBits(int bits,
                                     Function<String, Boolean> ok,
                                     int max) {
        int    i = 0;
        String uuidBits;
        do {
            uuidBits = getUUIDBits(bits);
            i++;
            if (i > max) return null;
        } while (!ok.apply(uuidBits));
        return uuidBits;
    }

}